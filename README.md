# Semantic Web APIs Hub #

Currently, there are several repositories and data models that provide descriptions for Web APIs. The main goal of the thesis is to establish an automated collection, transformation, and integration of several data sources with Web API descriptions.

Guidelines:

* Analyze and get familiar with existing datasets and data models for Web APIs.

* Establish mappings between different data models.

* For each individual data source, design and implement an automated collection, transformation, and integration into the unified data model. The Linked Web APIs model will provide the basis and if needed further extended.

* Validate and evaluate the knowledge extraction process for each identified Web API dataset.

## Requirements ##

* Java JDK 1.4 or above.

* Minimum 1GB RAM

## Installing Java JDK ##

Run in console

	sudo apt-get install default-jdk

## Automatic building ##

Download bash script from the following link: https://bitbucket.org/m1ci/semantic-web-apis-hub/downloads/build.sh

and run it in console




## ... OR ... ##




## Download repository ##

	git clone --recursive https://bitbucket.org/m1ci/semantic-web-apis-hub/

## Get RML library ##

#### Clone RML-Mapper ####

Clone RML-Mapper from https://github.com/RMLio/RML-Mapper using following commands:

	git clone --recursive https://github.com/RMLio/RML-Mapper.git
	git submodule update --init --recursive

#### Build RML-Mapper ####

Run

	mvn clean install -DskipTests
	
In RML-Processor/target/ folder get RML library file: RML-Processor-0.3.jar and copy into the root of the semantic-web-apis-hub folder

## Adding RML dependency ##
	
At first you need to install RML library (jar file) using Maven command:
	
	mvn install:install-file -Dfile=RML-Processor-0.3.jar -DgroupId=rml -DartifactId=rml -Dversion=0.3 -Dpackaging=jar

## Building project ##

The project must be built using Maven build manager:

	mvn clean install

Maven build manager will download and install all required dependencies. 

## Run Semantic APIs Hub ##

Extract specific API directory - (exec the command in the target folder - where is the Sem-API-Maven-1.jar)

	java -jar Sem-API-Maven-1.jar extract {pw, pw-mashup, apis-io, apis-guru, exicon, api-for-that}

Extract all API directories

	java -jar Sem-API-Maven-1.jar extract-all

Transform specific API data to RDF format

	java -jar Sem-API-Maven-1.jar transform {pw, pw-mashup, apis-io, apis-guru, exicon, api-for-that}

Transform all API data to RDF format

	java -jar Sem-API-Maven-1.jar transform-all
	
	
## RDF output ##

Google Drive link: https://drive.google.com/open?id=0BzIx5YW4ha-ldDJ6b2c5ZXF1R1k