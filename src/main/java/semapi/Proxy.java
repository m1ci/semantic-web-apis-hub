package semapi;

public class Proxy {

    public String ip;
    public int port;
    
    public Proxy(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }
    
}
