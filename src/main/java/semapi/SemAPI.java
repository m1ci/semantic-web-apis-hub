package semapi;

import java.io.IOException;
import parsers.Parser;

public class SemAPI {

    public static void main(String[] args) {

        ConsoleCommand command = null;
        DatasourceType datasource = null;

        /*
        command = ConsoleCommand.TRANSFORM_ALL;
        datasource = DatasourceType.ALL;
        */
        
        //Check for valid input
        if (args.length == 0) {
            System.out.println("Error: enter command (extract, extract-all, transform, transform-all) and datasource (pw, pw-mashup, apis-io, apis-guru, exicon, api-for-that).");
            return;
        }

        if (args.length > 2) {
            System.out.println("Error: too many arguments.");
            return;
        }

        if (args.length == 1) {
            String command_str = args[0].trim().toLowerCase();
            if (command_str.equals("extract-all")) {
                command = ConsoleCommand.EXTRACT_ALL;
                datasource = DatasourceType.ALL;
            }
            if (command_str.equals("transform-all")) {
                command = ConsoleCommand.TRANSFORM_ALL;
                datasource = DatasourceType.ALL;
            }
            if (command_str.equals("extract")) {
                System.out.println("Error: missed datasource argument.");
                return;
            }
            if (command_str.equals("transform")) {
                System.out.println("Error: missed datasource argument.");
                return;
            }
            if (command == null) {
                System.out.println("Error: wrong command argument.");
                return;
            }
        }

        if (args.length == 2) {
            String command_str = args[0].trim().toLowerCase();
            if (command_str.equals("extract")) {
                command = ConsoleCommand.EXTRACT;
            }
            if (command_str.equals("transform")) {
                command = ConsoleCommand.TRANSFORM;
            }
            if (command == null) {
                System.out.println("Error: wrong command argument.");
                return;
            }

            String datasource_str = args[1].trim().toLowerCase();
            if (datasource_str.equals("pw")) {
                datasource = DatasourceType.PW;
            }
            if (datasource_str.equals("pw-mashup")) {
                datasource = DatasourceType.PW_MASHUP;
            }
            if (datasource_str.equals("apis-io")) {
                datasource = DatasourceType.APIS_IO;
            }
            if (datasource_str.equals("apis-guru")) {
                datasource = DatasourceType.APIS_GURU;
            }
            if (datasource_str.equals("api-for-that")) {
                datasource = DatasourceType.API_FOR_THAT;
            }
            if (datasource_str.equals("exicon")) {
                datasource = DatasourceType.EXICON;
            }
            if (datasource == null) {
                System.out.println("Error: invalid datasource argument.");
                return;
            }
        }
        
                
        //Run application
        System.out.println("The aplication has been started.");

        Controller controller = new Controller();
        Parser.setMax(Env.qty);

        try {
            controller.run(command, datasource);
        } catch (IOException ex) {
            System.out.println("Error: error on application run.");
        }
        
        System.out.println("The aplication is finished.");
        
    }

}
