package semapi;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.List;
import org.bson.Document;

public class Filter {

    public static JsonObject documentToJson(List<Document> docList) {
        JsonObject newObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();

        for (Document doc : docList) {
            JsonObject jsonObject = (new JsonParser()).parse(doc.toJson()).getAsJsonObject();
            jsonArray.add(jsonObject);
        }

        newObject.add("data", jsonArray);

        return newObject;
    }

    public static boolean getBoolean(String str) {
        if (str.equalsIgnoreCase("yes") || str.equalsIgnoreCase("true")) {
            return true;
        }

        if (str.equalsIgnoreCase("no") || str.equalsIgnoreCase("false")) {
            return false;
        }

        return false;
    }

}
