package semapi;

import be.ugent.mmlab.rml.core.RMLEngine;
import be.ugent.mmlab.rml.core.StdRMLEngine;
import be.ugent.mmlab.rml.mapdochandler.extraction.std.StdRMLMappingFactory;
import be.ugent.mmlab.rml.mapdochandler.retrieval.RMLDocRetrieval;
import be.ugent.mmlab.rml.model.RMLMapping;
import be.ugent.mmlab.rml.model.dataset.RMLDataset;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;

public class TriplesEngine {

    public TriplesEngine() {
    }

    //Applying mapping rules, generate RDF and save to file
    public RMLDataset applyMapping(String mappingFile) {

        //Retrieve the Mapping Document
        RMLDocRetrieval mapDocRetrieval = new RMLDocRetrieval();
        Repository repository = mapDocRetrieval.getMappingDoc(mappingFile, RDFFormat.TURTLE);

        //Extracting the RML Mapping Definitions
        RMLEngine engine = new StdRMLEngine();
        StdRMLMappingFactory mappingFactory = new StdRMLMappingFactory();
        RMLMapping mapping = mappingFactory.extractRMLMapping(repository);

        //Applying the RML Mapping
        RMLDataset out = engine.chooseSesameDataSet("dataset", null, null);
        out = engine.runRMLMapping(out, mapping, null, null, null);

        //Set prefixes
        try {
            setNamespaces(out.getRepository().getConnection());
        } catch (RepositoryException ex) {
            Logger.getLogger(TriplesEngine.class.getName()).log(Level.SEVERE, null, ex);
        }

        return out;
    }

    private void setNamespaces(RepositoryConnection repCon) throws RepositoryException {

        repCon.setNamespace("dcterms", "http://purl.org/dc/terms/");
        repCon.setNamespace("foaf", "http://xmlns.com/foaf/0.1/");
        repCon.setNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        repCon.setNamespace("schema", "http://schema.org/");
        repCon.setNamespace("vcard", "http://www.w3.org/2006/vcard/ns#");
        repCon.setNamespace("doap", "http://usefulinc.com/ns/doap#");
        repCon.setNamespace("bbccore", "http://www.bbc.co.uk/ontologies/coreconcepts/");
        repCon.setNamespace("xsd", "http://www.w3.org/2001/XMLSchema#");
        repCon.setNamespace("ebucore", "http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#");
        repCon.setNamespace("lso", "http://linked-web-apis.fit.cvut.cz/ns/core#");
        repCon.setNamespace("ls", "http://linked-web-apis.fit.cvut.cz/resource/");
        repCon.setNamespace("pw", "http://linked-web-apis.fit.cvut.cz/resource/pw/");
        repCon.setNamespace("ex", "http://linked-web-apis.fit.cvut.cz/resource/ex/");
        repCon.setNamespace("ag", "http://linked-web-apis.fit.cvut.cz/resource/ag/");
        repCon.setNamespace("a4t", "http://linked-web-apis.fit.cvut.cz/resource/a4t/");
        repCon.setNamespace("io", "http://linked-web-apis.fit.cvut.cz/resource/io/");
        repCon.setNamespace("prov", "http://www.w3.org/ns/prov#");
        
    }

}
