package semapi;

import java.lang.reflect.Field;
import org.bson.Document;

public abstract class Data {

    public Document toMongo() {
        Document doc = new Document();

        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                doc.put(field.getName(), field.get(this));
            } catch (IllegalAccessException ex) {
                System.out.println("Error in toMongo method");
            }
        }

        return doc;
    }

    public void print() {
        System.out.println("Data:");

        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                System.out.print(field.getName() + ": ");
                System.out.println(field.get(this));
            } catch (IllegalAccessException ex) {
                System.out.println("Error on data object printing: " + ex);
            }
        }

        System.out.println();
    }

}
