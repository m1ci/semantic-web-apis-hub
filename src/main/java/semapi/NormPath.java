package semapi;

public class NormPath {

    public String path;
    public Norm norm;

    public NormPath(String _path, Norm _norm) {
        path = _path;
        norm = _norm;
    }

}
