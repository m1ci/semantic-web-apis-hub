package semapi;

import java.util.ArrayList;
import java.util.List;

public class NormData {

    public boolean added = false;
    public List<String> values = new ArrayList();

    public void reset() {
        added = false;
        values.clear();
    }

    public void add(String str) {
        values.add(str);
        added = true;
    }

}
