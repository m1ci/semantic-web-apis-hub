package semapi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.apache.commons.lang.StringUtils.strip;
import static org.apache.commons.lang.StringUtils.strip;

public class Normalization {

    public static JsonElement toJsonObject(Object obj) {
        Gson gson = new Gson();
        return gson.toJsonTree(obj);
    }

    public static String toSlug(String s) {
        
        String str = s.trim().toLowerCase();
        String norm_str = "";
        
        for (int i = 0; i < str.length(); i++) {
            
            if (((int)str.charAt(i)) >= 48 && ((int)str.charAt(i)) <= 57 && i == 0) {
                norm_str += "the-";
            }
            
            if ((((int)str.charAt(i)) >= 97 && ((int)str.charAt(i)) <= 122) ||
                (((int)str.charAt(i)) >= 48 && ((int)str.charAt(i)) <= 57)) {
                norm_str += str.charAt(i);
            } else {
                norm_str += '-';
            }
        }
        
        norm_str = strip(norm_str, "-");
        
        return strip(norm_str, "-").replaceAll("-{2,}", "-");
    }

    public static String toSiteName(String s) {
        return strip(s.trim(), "/").toLowerCase().replace("https://", "").replace("http://", "").replace("www.", "").replace(" ", "-").replace("/", "-").replace(":", "-").replace(",", "-").replaceAll("-{2,}", "-");
    }

    public static String toDate(String s, String fromFormat, String toFormat) {
        SimpleDateFormat from = new SimpleDateFormat(fromFormat);

        try {
            Date d = from.parse(s);
            SimpleDateFormat to = new SimpleDateFormat(toFormat);
            return to.format(d);
        } catch (ParseException ex) {
            Logger.getLogger(Normalization.class.getName()).log(Level.SEVERE, null, ex);
            return s;
        }
    }

    public static String toPrettyJsonString(JsonElement obj) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(obj);
    }

    public static JsonElement normalize(JsonElement element, List<NormPath> fields) {
        String prefix = "_normalized";
        NormData normData = new NormData();
        JsonElement newElement = recursiveNormalization("", element, fields, "", normData, prefix);
        return newElement;
    }

    private static JsonElement recursiveNormalization(String key, JsonElement element, List<NormPath> fields, String path, NormData normData, String prefix) {
        if (element.isJsonObject()) {
            JsonObject newObject = new JsonObject();

            for (Map.Entry<String, JsonElement> entry : element.getAsJsonObject().entrySet()) {
                normData.reset();
                JsonElement obj = recursiveNormalization(entry.getKey(), entry.getValue(), fields, path + "/" + entry.getKey(), normData, prefix);
                newObject.add(entry.getKey(), obj);

                if (normData.added) {
                    if (!obj.isJsonArray()) {
                        if (!normData.values.isEmpty()) {
                            newObject.addProperty(entry.getKey() + prefix, normData.values.get(0)); // + "_normalization"
                        }
                    } else {
                        JsonArray array = new JsonArray();
                        for (String s : normData.values) {
                            array.add(s);
                        }

                        newObject.add(entry.getKey() + prefix, array); // + "_normalization"
                    }
                }
            }

            return newObject;
        } else {
            if (element.isJsonArray()) {
                JsonArray newArray = new JsonArray();
                JsonArray jsonArray = element.getAsJsonArray();

                if (jsonArray.size() == 0) {
                    for (NormPath s : fields) {
                        if ((strip(path.toLowerCase(), "/") + "/*").equals(s.path.toLowerCase())) {
                            normData.added = true;
                        }
                    }
                } else {
                    for (JsonElement arrayElement : jsonArray) {
                        JsonElement obj = recursiveNormalization(key, arrayElement, fields, path + "/*", normData, prefix);
                        newArray.add(obj);
                    }
                }

                return newArray;
            } else {
                if (element.isJsonPrimitive()) {
                    JsonPrimitive primitive = element.getAsJsonPrimitive();

                    if (primitive.isString()) {
                        for (NormPath s : fields) {
                            if (strip(path.toLowerCase(), "/").equals(s.path.toLowerCase())) {
                                if (s.norm == Norm.toSlug) {
                                    normData.add(Normalization.toSlug(primitive.getAsString()));
                                }

                                if (s.norm == Norm.toSite) {
                                    normData.add(Normalization.toSiteName(primitive.getAsString()));
                                }

                                return primitive;
                            }
                        }
                    }

                    return primitive;
                }
            }
        }

        return element;
    }

    public static boolean createArray(JsonElement element, String path) {
        path = strip(path, "/");
        String[] paths = path.split("/");
        String[] only_path = Arrays.copyOf(paths, paths.length - 1);
        String propName = paths[paths.length - 1];
        return addArrayElement(element, only_path, 0, "", propName);
    }

    private static boolean addArrayElement(JsonElement element, String[] paths, int pos, String path, String propName) {

        boolean res = false;

        if (pos == paths.length) {
            
            if (!element.getAsJsonObject().has(propName)) {
                element.getAsJsonObject().add(propName, new JsonArray());
                return true;
            } else {
                if (!element.getAsJsonObject().get(propName).isJsonArray()) {
                    element.getAsJsonObject().remove(propName);
                    element.getAsJsonObject().add(propName, new JsonArray());
                    return true;
                }
            }

            return false;
        }

        if (element.isJsonObject()) {
            if (paths[pos].equals("*")) {
                for (Map.Entry<String, JsonElement> entry : element.getAsJsonObject().entrySet()) {
                    res = addArrayElement(entry.getValue(), paths, pos + 1, path + "/" + entry.getKey(), propName);
                }
            } else if (element.getAsJsonObject().has(paths[pos])) {
                res = addArrayElement(element.getAsJsonObject().get(paths[pos]), paths, pos + 1, path + "/" + paths[pos], propName);
            }
        } else {
            if (element.isJsonArray()) {
                for (JsonElement elem : element.getAsJsonArray()) {
                    res = addArrayElement(elem, paths, pos + 1, path + "/*", propName);
                }
            }
        }

        return res;
    }

}
