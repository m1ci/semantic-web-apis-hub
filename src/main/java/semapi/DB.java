package semapi;

import com.google.gson.JsonElement;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class DB {

    private static final MongoDatabase db;

    static {
        MongoClient mongo = new MongoClient(Env.dbURI, Env.dbPort);
        db = mongo.getDatabase(Env.db);
    }

    //Insert json into collection
    public static boolean insert_json(String collection_str, JsonElement jsonObject) {

        MongoCollection<Document> collection = db.getCollection(collection_str);

        if (jsonObject.isJsonArray()) {
            for (JsonElement arrayElement : jsonObject.getAsJsonArray()) {
                collection.insertOne(Document.parse(arrayElement.toString()));
            }
            return true;
        }

        if (jsonObject.isJsonObject()) {
            collection.insertOne(Document.parse(jsonObject.toString()));
            return true;
        }

        return false;
    }

    //Update collection
    public static boolean update_json(String collection_str, JsonElement jsonObject) {
        delete_all_api(collection_str);
        return insert_json(collection_str, jsonObject);
    }

    //Delete all apis from db
    public static void delete_all_api(String collection_str) {
        MongoCollection<Document> table = db.getCollection(collection_str);
        table.deleteMany(new Document());
    }

}
