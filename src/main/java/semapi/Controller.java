package semapi;

import be.ugent.mmlab.rml.model.dataset.RMLDataset;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import parsers.Parser;
import parsers.apiParsers.APIS_GURU_Parser;
import parsers.apiParsers.APIS_Parser;
import parsers.apiParsers.API_FOR_THAT_Parser;
import parsers.apiParsers.EXICON_Parser;
import parsers.apiParsers.PWEB_Mashup_Parser;
import parsers.apiParsers.PWEB_Parser;

public class Controller {

    public Controller() {
    }

    private JsonElement runParser(Parser parser, Object storage) {

        if (!(storage instanceof Iterable)) {
            System.out.println("Error: current storage is not iterable object");
            return null;
        }

        boolean res = parser.crawl(storage);
        if (!res) {
            System.out.println("Error: occured on crawling");
            return null;
        }

        JsonArray newArray = new JsonArray();
        Iterator iterator = ((Iterable) storage).iterator();
        while (iterator.hasNext()) {
            JsonElement jsonObject = parser.runParse(iterator.next());
            if (jsonObject != null) {
                if (jsonObject.isJsonArray()) {
                    newArray.addAll(jsonObject.getAsJsonArray());
                } else {
                    if (jsonObject.isJsonObject()) {
                        newArray.add(jsonObject.getAsJsonObject());
                    }
                }
            } else {
                System.out.println("Error: occured on parsing");
            }
        }
        System.out.println("Parsing (" + parser.getName() + ") has finished");
        return newArray;
    }

    
    //Apply mapping and generate RDF
    private void applyMapping(String mapping_file, String rdf_file) {
        TriplesEngine triplesEngine = new TriplesEngine();
        RMLDataset dataset = triplesEngine.applyMapping(mapping_file);
        StorageEngine.saveToFile(dataset, rdf_file, Env.rdf_format);
        
        if (Env.rdf_print == 1) {
            StorageEngine.print(dataset, Env.rdf_format);
        }
    }

    public void run(ConsoleCommand command, DatasourceType datasource) throws IOException {

        if (datasource == DatasourceType.API_FOR_THAT || datasource == DatasourceType.ALL) {
            if (command == ConsoleCommand.EXTRACT || command == ConsoleCommand.EXTRACT_ALL) {

                JsonElement jsonObject = runParser(new API_FOR_THAT_Parser("api-for-that", Env.is_proxy), new HashSet<>());
                if (jsonObject != null) {
                    /*if (!DB.update_json("api_for_that", jsonObject)) {
                     System.out.println("Error: occured on saving json object to db");
                     } 
                     */
                    StorageEngine.jsonToFile(jsonObject, "data/apiforthat/api_for_that.json");
                }
            }
            if (command == ConsoleCommand.TRANSFORM || command == ConsoleCommand.TRANSFORM_ALL) {
                applyMapping("data/apiforthat/api_for_that_rml.ttl", "data/apiforthat/api_for_that_rdf.ttl");
            }
        }

        if (datasource == DatasourceType.APIS_GURU || datasource == DatasourceType.ALL) {
            if (command == ConsoleCommand.EXTRACT || command == ConsoleCommand.EXTRACT_ALL) {

                JsonElement jsonObject = runParser(new APIS_GURU_Parser("apis.guru"), new HashSet<>());
                if (jsonObject != null) {
                    /*
                     if (!DB.update_json("apis_guru", jsonObject)) {
                     System.out.println("Error: occured on saving json object to db");
                     }
                     */
                    StorageEngine.jsonToFile(jsonObject, "data/apis-guru/apis_guru.json");
                }
            }
            if (command == ConsoleCommand.TRANSFORM || command == ConsoleCommand.TRANSFORM_ALL) {
                applyMapping("data/apis-guru/apis_guru_rml.ttl", "data/apis-guru/apis_guru_rdf.ttl");
            }
        }

        if (datasource == DatasourceType.APIS_IO || datasource == DatasourceType.ALL) {
            if (command == ConsoleCommand.EXTRACT || command == ConsoleCommand.EXTRACT_ALL) {

                JsonElement jsonObject = runParser(new APIS_Parser("apis.io"), new HashSet<>());
                if (jsonObject != null) {
                    /*if (!DB.update_json("apis_io", jsonObject)) {
                     System.out.println("Error: occured on saving json object to db");
                     }
                     */
                    StorageEngine.jsonToFile(jsonObject, "data/apis.io/apis_io.json");
                }
            }
            if (command == ConsoleCommand.TRANSFORM || command == ConsoleCommand.TRANSFORM_ALL) {
                applyMapping("data/apis.io/apis_io_rml.ttl", "data/apis.io/apis_io_rdf.ttl");
            }
        }

        if (datasource == DatasourceType.EXICON || datasource == DatasourceType.ALL) {
            if (command == ConsoleCommand.EXTRACT || command == ConsoleCommand.EXTRACT_ALL) {

                JsonElement jsonObject = runParser(new EXICON_Parser("exicon"), new HashSet<>());
                if (jsonObject != null) {
                    /*if (!DB.update_json("exicon", jsonObject)) {
                     System.out.println("Error: occured on saving json object to db");
                     }
                     */
                    StorageEngine.jsonToFile(jsonObject, "data/exiconglobal/exicon.json");
                }
            }
            if (command == ConsoleCommand.TRANSFORM || command == ConsoleCommand.TRANSFORM_ALL) {
                applyMapping("data/exiconglobal/exicon_rml.ttl", "data/exiconglobal/exicon_rdf.ttl");
            }
        }

        if (datasource == DatasourceType.PW || datasource == DatasourceType.ALL) {
            if (command == ConsoleCommand.EXTRACT || command == ConsoleCommand.EXTRACT_ALL) {

                JsonElement jsonObject = runParser(new PWEB_Parser("programmable-web", Env.is_proxy), new JsonArray());
                if (jsonObject != null) {
                    /*if (!DB.update_json("pweb", jsonObject)) {
                     System.out.println("Error: occured on saving json object to db");
                     }
                     */
                    StorageEngine.jsonToFile(jsonObject, "data/programmableweb/pweb.json");
                }
            }
            if (command == ConsoleCommand.TRANSFORM || command == ConsoleCommand.TRANSFORM_ALL) {
                applyMapping("data/programmableweb/pweb_rml.ttl", "data/programmableweb/pweb_rdf.ttl");
            }
        }

        if (datasource == DatasourceType.PW_MASHUP || datasource == DatasourceType.ALL) {
            if (command == ConsoleCommand.EXTRACT || command == ConsoleCommand.EXTRACT_ALL) {
                JsonElement jsonObject = runParser(new PWEB_Mashup_Parser("programmable-web-mashup", Env.is_proxy), new JsonArray());
                if (jsonObject != null) {
                    /*if (!DB.update_json("pweb-mashup", jsonObject)) {
                     System.out.println("Error: occured on saving json object to db");
                     }
                     */
                    StorageEngine.jsonToFile(jsonObject, "data/programmableweb/pweb_mashup.json");
                }
            }
            if (command == ConsoleCommand.TRANSFORM || command == ConsoleCommand.TRANSFORM_ALL) {
                applyMapping("data/programmableweb/pweb_mashup_rml.ttl", "data/programmableweb/pweb_mashup_rdf.ttl");
            }
        }

    }

}
