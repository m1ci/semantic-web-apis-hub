package semapi;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import javax.imageio.ImageIO;

public class ImageProcessing {

    public static String downloadImage(String imageUrl, String destinationFile, String filename) {
        try {
            String format = "png";
            if (imageUrl.contains(".png")) {
                format = "png";
            }

            if (imageUrl.contains(".jpg")) {
                format = "jpg";
            }

            if (imageUrl.contains(".jpeg")) {
                format = "jpeg";
            }

            URLConnection connection = new URL(imageUrl).openConnection();
            //connection.setRequestProperty("User-Agent", UserAgent.Next());
            connection.connect();

            try (InputStream is = connection.getInputStream()) {
                BufferedImage img = ImageIO.read(is);
                File outputfile = new File(destinationFile + filename + "." + format);
                ImageIO.write(img, format, outputfile);

                return destinationFile + filename + "." + format;
            }

        } catch (IOException ex) {
            System.out.println("Error on image downloading: " + ex);
            return "";
        }
    }

}
