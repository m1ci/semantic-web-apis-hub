package semapi;

public enum ConsoleCommand {

    EXTRACT,
    EXTRACT_ALL,
    TRANSFORM,
    TRANSFORM_ALL

}
