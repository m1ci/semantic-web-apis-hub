package semapi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ProxyGenerator {

    private static final List<Proxy> proxyList = new ArrayList();
    private static int i = -1;

    static {

        proxyList.clear();
        try (BufferedReader br = new BufferedReader(new FileReader(Env.proxy_path))) {
            String line;
            while ((line = br.readLine()) != null) {

                String[] line_parsed = line.split(":");
                if (line_parsed.length == 2) {

                    String ip = line_parsed[0];
                    int port = Integer.parseInt(line_parsed[1]);
                    proxyList.add(new Proxy(ip, port));

                }

            }
        } catch (Exception ex) {
            System.out.println("Cannot read proxy file");
        }

        /*
        System.out.println("Proxy size: " + proxyList.size());
        for (Proxy proxy : proxyList) {
            System.out.println("Proxy: " + proxy.ip + ":" + proxy.port);
        }
        */
        
    }

    public static Proxy Next() {
        if (proxyList.isEmpty()) {
            return null;
        }

        if (++i >= proxyList.size()) {
            i = 0;
        }

        return proxyList.get(i);
    }

}
