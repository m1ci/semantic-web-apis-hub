/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semapi;

import be.ugent.mmlab.rml.model.dataset.RMLDataset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import org.openrdf.rio.RDFFormat;

/**
 *
 * @author owner
 */
public class StorageEngine {
 
    //Save generated RDF to file
    public static void saveToFile(RMLDataset dataset, String outputFile, String format) {
        if (dataset == null) {
            return;
        }

        try (OutputStream out = new FileOutputStream(outputFile)) {
            dataset.dumpRDF(out, getFormat(format));
        } catch (IOException ex) {
            System.out.println("Error printing in file: " + ex);
        }
    }

    private static RDFFormat getFormat(String format) {

        RDFFormat rdf_format;
        String format_clr = format.toLowerCase().trim();
        switch (format_clr) {
            case "turtle":
                rdf_format = RDFFormat.TURTLE;
                break;
            case "ntriples":
                rdf_format = RDFFormat.NTRIPLES;
                break;
            case "binary":
                rdf_format = RDFFormat.BINARY;
                break;
            case "jsonld":
                rdf_format = RDFFormat.JSONLD;
                break;
            case "n3":
                rdf_format = RDFFormat.N3;
                break;
            case "nquads":
                rdf_format = RDFFormat.NQUADS;
                break;
            case "rdfa":
                rdf_format = RDFFormat.RDFA;
                break;
            case "rdfjson":
                rdf_format = RDFFormat.RDFJSON;
                break;
            case "rdfxml":
                rdf_format = RDFFormat.RDFXML;
                break;
            case "trig":
                rdf_format = RDFFormat.TRIG;
                break;
            case "trix":
                rdf_format = RDFFormat.TRIX;
                break;
            default:
                rdf_format = RDFFormat.TURTLE;
        }

        return rdf_format;
    }
    
    
    //Print generated RDF to console
    public static void print(RMLDataset dataset, String format) {
        if (dataset == null) {
            System.out.println("Nothing to print");
            return;
        }

        dataset.dumpRDF(System.out, getFormat(format));
    }

    
    //Save Json object to file
    public static void jsonToFile(JsonElement jsonObject, String path) throws IOException {
        try (Writer writer = new FileWriter(path)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(jsonObject, writer);
        }
    }

}
