package semapi;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Env extends Data {

    public static final String path = "data/env.xml";
    public static String db;
    public static String dbURI;
    public static int dbPort;
    public static String dbUser;
    public static String dbPass;
    public static String rdf_format;
    public static String logoPath;
    public static int timeout;
    public static int rdf_print;
    public static int qty;
    public static String proxy_path;
    public static int request_number;
    public static int delay;
    public static boolean is_proxy;
    
    static {
        try {
            File xmlFile = new File(path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();

            db = doc.getElementsByTagName("db").item(0).getTextContent();
            dbUser = doc.getElementsByTagName("dbUser").item(0).getTextContent();
            dbPass = doc.getElementsByTagName("dbPass").item(0).getTextContent();
            rdf_format = doc.getElementsByTagName("rdf_format").item(0).getTextContent();
            dbURI = doc.getElementsByTagName("dburi").item(0).getTextContent();
            dbPort = Integer.parseInt(doc.getElementsByTagName("dbport").item(0).getTextContent());
            logoPath = doc.getElementsByTagName("logoPath").item(0).getTextContent();
            timeout = Integer.parseInt(doc.getElementsByTagName("timeout").item(0).getTextContent());
            rdf_print = Integer.parseInt(doc.getElementsByTagName("rdf_print").item(0).getTextContent());
            qty = Integer.parseInt(doc.getElementsByTagName("qty").item(0).getTextContent());
            proxy_path = doc.getElementsByTagName("proxy").item(0).getTextContent();
            request_number = Integer.parseInt(doc.getElementsByTagName("requestNumber").item(0).getTextContent());
            delay = Integer.parseInt(doc.getElementsByTagName("delay").item(0).getTextContent());
            is_proxy = Boolean.parseBoolean(doc.getElementsByTagName("isProxy").item(0).getTextContent());
            
        } catch (ParserConfigurationException | SAXException | IOException | DOMException | NumberFormatException ex) {
            System.out.println("Error on reading " + path + " file: " + ex);
        }
    }

}
