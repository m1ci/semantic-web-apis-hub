package parsers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import semapi.Env;
import semapi.Proxy;
import semapi.ProxyGenerator;
import semapi.UserAgent;

public abstract class HtmlParser extends Parser {

    private Proxy proxy;
    
    public HtmlParser(String name, boolean proxyEnabled) {
        super(name, proxyEnabled);
    }

    protected Document readHtmlFromUrl(String url) {
        try {
                    
            if (proxyEnabled) {
                
                if ((proxyRequestIndex++) % Env.request_number == 0) {
                    proxy = ProxyGenerator.Next();
                    System.out.println("Proxy changed: " + proxy.ip + ":" + proxy.port);
                }
                
                Document doc = Jsoup.connect(url).proxy(proxy.ip, proxy.port).userAgent(UserAgent.Next()).timeout(Env.timeout).get();
                return doc;
            } else {
                Document doc = Jsoup.connect(url).userAgent(UserAgent.Next()).timeout(Env.timeout).get();
                try {
                    TimeUnit.MILLISECONDS.sleep(Env.delay);
                } catch (InterruptedException ex) {
                    System.out.println("Error: delay interruption");
                }                
                return doc;
            }
            
        } catch (IOException ex) {
            System.out.println("Error: cannot get html data from url");
            return null;
        }
    }

    protected JsonElement parseString(String str) {

        JsonArray array = new JsonArray();
        String[] arr = str.split(",");

        for (String s : arr) {
            String ss = s.trim();
            if (!ss.isEmpty()) {
                array.add(ss);
            }
        }

        return array;
    }

}
