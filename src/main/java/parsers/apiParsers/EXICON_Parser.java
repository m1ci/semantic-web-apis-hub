package parsers.apiParsers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import parsers.JsnParser;
import semapi.Norm;
import semapi.NormPath;

public class EXICON_Parser extends JsnParser {

    public EXICON_Parser(String name) {
        super(name);
    }

    @Override
    protected List<String> creation() {
        List<String> toCreate = new ArrayList();
        toCreate.add("*/dataFormat");
        toCreate.add("*/protocol");
        toCreate.add("*/tags");
        return toCreate;
    }

    @Override
    protected List<NormPath> normalization() {
        List<NormPath> toNormalize = new ArrayList();
        toNormalize.add(new NormPath("*/apiName", Norm.toSlug));
        toNormalize.add(new NormPath("*/authModel", Norm.toSlug));
        toNormalize.add(new NormPath("*/usageFee", Norm.toSlug));
        toNormalize.add(new NormPath("*/apiCategory/*/name", Norm.toSlug));
        toNormalize.add(new NormPath("*/dataFormat/*", Norm.toSlug));
        toNormalize.add(new NormPath("*/protocol/*", Norm.toSlug));
        toNormalize.add(new NormPath("*/tags/*", Norm.toSlug));
        return toNormalize;
    }

    @Override
    public boolean crawl(Object urls) {
        trustToCertificates();
        int show = 50;
        StringBuilder jsonStr = new StringBuilder();
        boolean res = readJsonFromUrl("https://app.exiconglobal.com/apis?terms=&companyId=&show=1&page=1&status=enable", jsonStr);

        if (!res) {
            return false;
        }

        JsonParser parser = new JsonParser();
        JsonObject rootObj = parser.parse(jsonStr.toString()).getAsJsonObject();

        int qty = rootObj.get("count").getAsInt();
        int pages = (int) Math.ceil((double) qty / show);

        int cur = 0;
        while (cur < pages) {

            if (((cur + 1) * show) >= max && max != 0) {
                String url = "https://app.exiconglobal.com/apis?terms=&companyId=&show=" + (max - cur * show) + "&page=" + (cur + 1) + "&status=enable";
                ((Set) urls).add(url);
                System.out.println("Crawled url: " + url);
                break;
            }

            String url = "https://app.exiconglobal.com/apis?terms=&companyId=&show=" + show + "&page=" + (cur + 1) + "&status=enable";
            ((Set) urls).add(url);
            cur++;
            System.out.println("Crawled url: " + url);
        }

        return true;
    }

    @Override
    protected JsonElement parse(Object obj) {
        String url = (String) obj;
        StringBuilder jsonStr = new StringBuilder();
        boolean res = readJsonFromUrl(url, jsonStr);

        if (!res) {
            return null;
        }

        JsonParser parser = new JsonParser();
        JsonObject rootObj = parser.parse(jsonStr.toString()).getAsJsonObject();
        JsonArray dataArray = (JsonArray) rootObj.get("data");

        System.out.println("Parsed: " + url);
        return dataArray;
    }

    private void trustToCertificates() {
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    //No need to implement.
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    //No need to implement.
                }
            }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
