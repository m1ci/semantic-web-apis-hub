package parsers.apiParsers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import parsers.JsnParser;
import semapi.Norm;
import semapi.NormPath;

public class APIS_Parser extends JsnParser {

    public APIS_Parser(String name) {
        super(name);
    }

    @Override
    protected List<String> creation() {
        List<String> toCreate = new ArrayList();
        toCreate.add("*/tags");
        return toCreate;
    }

    @Override
    protected List<NormPath> normalization() {
        List<NormPath> toNormalize = new ArrayList();
        toNormalize.add(new NormPath("*/name", Norm.toSlug));
        toNormalize.add(new NormPath("*/tags/*", Norm.toSlug));
        return toNormalize;
    }

    @Override
    public boolean crawl(Object urls) {
        ((Set) urls).add(max == 0 ? "http://apis.io/api/apis?limit=1500" : "http://apis.io/api/apis?limit=" + max);
        return true;
    }

    @Override
    protected JsonElement parse(Object obj) {
        String url = (String) obj;
        StringBuilder jsonStr = new StringBuilder();
        boolean res = readJsonFromUrl(url, jsonStr);

        if (!res) {
            return null;
        }

        JsonParser parser = new JsonParser();
        JsonObject rootObject = parser.parse(jsonStr.toString()).getAsJsonObject();
        JsonElement jsonObject = rootObject.get("data");

        System.out.println("Parsed: " + url);
        return jsonObject;
    }

}
