package parsers.apiParsers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import parsers.HtmlParser;
import semapi.Norm;
import semapi.NormPath;
import semapi.Normalization;

public class PWEB_Mashup_Parser extends HtmlParser {

    private int count = 0;

    public PWEB_Mashup_Parser(String name, boolean proxyEnabled) {
        super(name, proxyEnabled);
    }

    
    @Override
    protected List<NormPath> normalization() {
        List<NormPath> toNormalize = new ArrayList();
        toNormalize.add(new NormPath("name", Norm.toSlug));
        toNormalize.add(new NormPath("company", Norm.toSlug));
        toNormalize.add(new NormPath("type", Norm.toSlug));
        toNormalize.add(new NormPath("categories/*", Norm.toSlug));
        toNormalize.add(new NormPath("apis/*", Norm.toSlug));
        toNormalize.add(new NormPath("tags/*", Norm.toSlug));
        return toNormalize;
    }
    
    
    @Override
    protected List<String> creation() {
        List<String> toCreate = new ArrayList();
        toCreate.add("categories");
        toCreate.add("apis");
        toCreate.add("tags");
        //toCreate.add("company");
        //toCreate.add("type");
        return toCreate;
    }
    
    
    @Override
    public boolean crawl(Object obj) {

        JsonElement apis = (JsonElement) obj;
        int page = 0;
        int size = 0;

        while (true) {
            String url = "http://www.programmableweb.com/category/all/mashups?page=" + (page++) + "&deadpool=1";
            Document htmlDocument = readHtmlFromUrl(url);

            if (htmlDocument == null) {
                System.out.println("Skipped: " + url);
                continue;
            }

            Element table = htmlDocument.getElementsByClass("views-table").first();
            if (table == null) {
                return true;
            }

            Iterator<Element> iterator = table.select("tbody > tr").iterator();
            while (iterator.hasNext()) {
                Element tr = iterator.next();

                JsonObject api = new JsonObject();

                Element elem_date = tr.select("td:eq(3)").first();
                if (elem_date != null) {
                    api.addProperty("created", Normalization.toDate(elem_date.text().trim(), "MM.dd.yyyy", "yyyy-MM-dd"));
                }

                Element elem_url = tr.select("td:eq(0) > a[href]").first();
                if (elem_url != null) {
                    api.addProperty("url", elem_url.attr("abs:href"));
                    api.addProperty("name", elem_url.text());
                    apis.getAsJsonArray().add(api);
                }

                if (++size >= max && max != 0) {
                    System.out.println("Crawled url: " + url);
                    return true;
                }
            }

            System.out.println("Crawled url: " + url);
        }
    }

    @Override
    protected JsonElement parse(Object apiData) {

        JsonObject api = ((JsonElement) apiData).getAsJsonObject();
        
        Document htmlDocument = readHtmlFromUrl(api.getAsJsonObject().get("url").getAsString());

        if (htmlDocument == null) {
            return null;
        }

        Element apiDescription = htmlDocument.select("div[class=\"intro\"]").first();
        if (apiDescription != null) {
            api.addProperty("description", apiDescription.text().trim());
        }

        Element apiDeprecated = htmlDocument.select("div[class=\"deprecated\"]").first();
        if (apiDeprecated != null) {
            api.addProperty("isDeprecated", apiDeprecated.ownText().trim().contains("deprecated"));
        }

        Element imageUrl = htmlDocument.select("div[class=\"field-items\"] > div > img").first();
        if (imageUrl != null) {
            api.addProperty("logo_url", imageUrl.absUrl("src"));
            //apiData.logo_local_url = ImageProcessing.downloadImage(apiData.logo_url, Env.logoPath, apiData.name.replace(" ", "-").trim());
        }

        Iterator<Element> apiIterator = htmlDocument.select("div[class=\"tags\"] > a").iterator();
        Element tagA;
        JsonArray array = new JsonArray();
        while (apiIterator.hasNext()) {
            tagA = apiIterator.next();
            array.add(tagA.text().trim());
        }
        
        api.add("tags", array);
        
        
        //Get API data from table
        Iterator<Element> iterator = htmlDocument.select("div[class=\"section specs\"] > div").iterator();
        Element div;
        while (iterator.hasNext()) {
            div = iterator.next();
            String label = div.select("label").text().trim().toLowerCase();
            String span = div.select("span").text().trim();

            if (label.contains("related apis")) {
                api.add("apis", parseString(span).getAsJsonArray());
            }

            if (label.contains("categories")) {
                api.add("categories", parseString(span).getAsJsonArray());
            }
            
            if (label.contains("url")) {
                api.addProperty("api_url", span);
            }

            if (label.contains("company")) {
                //api.add("company", parseString(span).getAsJsonArray());
                api.addProperty("company", span);
            }

            if (label.contains("mashup/app type")) {
                //api.add("type", parseString(span).getAsJsonArray());
                api.addProperty("type", span);
            }

            
        }

        System.out.println((++count) + " - parsed: " + api.getAsJsonObject().get("url").getAsString());
        
        return api;
    }

}
