package parsers.apiParsers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import parsers.JsnParser;
import semapi.Norm;
import semapi.NormPath;

public class APIS_GURU_Parser extends JsnParser {

    public APIS_GURU_Parser(String name) {
        super(name);
    }

    @Override
    protected List<String> creation() {
        List<String> toCreate = new ArrayList();
        toCreate.add("*/schemes");
        toCreate.add("*/versions/data/info/x-tags");
        toCreate.add("*/versions/data/info/x-apisguru-categories");
        return toCreate;
    }

    @Override
    protected List<NormPath> normalization() {
        List<NormPath> toNormalize = new ArrayList();
        toNormalize.add(new NormPath("*/_name", Norm.toSlug));
        toNormalize.add(new NormPath("*/schemes/*", Norm.toSlug));
        toNormalize.add(new NormPath("*/versions/data/info/x-tags/*", Norm.toSlug));
        toNormalize.add(new NormPath("*/versions/data/info/x-apisguru-categories/*", Norm.toSlug));
        return toNormalize;
    }

    @Override
    public boolean crawl(Object urls) {
        ((Set) urls).add("https://api.apis.guru/v2/list.json");
        return true;
    }

    @Override
    protected JsonElement parse(Object obj) {
        String url = (String) obj;
        StringBuilder jsonStr = new StringBuilder();
        boolean res = readJsonFromUrl(url, jsonStr);

        if (!res) {
            return null;
        }

        JsonParser parser = new JsonParser();
        JsonElement rootObj = parser.parse(jsonStr.toString()).getAsJsonObject();

        //Go through all apis
        JsonArray jsonArray = new JsonArray();
        int i = 0;
        for (Map.Entry<String, JsonElement> entry : rootObj.getAsJsonObject().entrySet()) {
            StringBuilder swaggerUrl = new StringBuilder();
            JsonElement newObject = recursiveReplace("", entry.getValue(), swaggerUrl);

            //Add API name
            newObject.getAsJsonObject().addProperty("_name", entry.getKey());

            
            if (swaggerUrl.length() > 0) {
                StringBuilder swaggerJsonStr = new StringBuilder();
                boolean result = readJsonFromUrl(swaggerUrl.toString(), swaggerJsonStr);
                if (result) {
                    
                    try {
                        
                        JsonElement swaggerTree = parser.parse(swaggerJsonStr.toString()).getAsJsonObject();
                    
                        if (swaggerTree.getAsJsonObject().has("schemes")) {
                            newObject.getAsJsonObject().add("schemes", swaggerTree.getAsJsonObject().get("schemes"));
                        }
                        if (swaggerTree.getAsJsonObject().has("host")) {
                            newObject.getAsJsonObject().add("host", swaggerTree.getAsJsonObject().get("host"));
                        }
                        if (swaggerTree.getAsJsonObject().has("basePath")) {
                            newObject.getAsJsonObject().add("basePath", swaggerTree.getAsJsonObject().get("basePath"));
                        }
                        
                        System.out.println("Parsed swagger url: " + swaggerUrl.toString());

                    } catch (com.google.gson.JsonSyntaxException ex) {
                        System.out.println("Parsed swagger url (error): " + ex.toString());
                    }
                    
                }
            }

            
            jsonArray.add(newObject);

            if (++i >= max && max != 0) {
                break;
            }
        }

        System.out.println("Parsed: " + url);
        return jsonArray;
    }

    /*
     private JsonElement getJsonElement(JsonElement jsonElement, String path) {
        
     String[] paths = path.split("/");
     int i = 0;
     JsonElement currentElement = jsonElement;
     while (i < paths.length) {
            
     if (i == paths.length) {
     return currentElement;
     }
            
     if (currentElement.isJsonObject() && currentElement.getAsJsonObject().has(paths[i])) {
     currentElement = currentElement.getAsJsonObject().get(paths[i]);
     }
     else {
     if (currentElement.isJsonArray() && paths[i].matches("\\d+")) {
     int pos = Integer.parseInt(paths[i]);
     if (pos < currentElement.getAsJsonArray().size()) {
     currentElement = currentElement.getAsJsonArray().get(pos);
     }
     }
     }
            
     i++;
     }
        
     return null;
     }
     */
    //Recursively go through json object and change keys (MongoDB does not accept dots in field's name)
    private JsonElement recursiveReplace(String key, JsonElement element, StringBuilder swagger) {
        JsonElement newObject = new JsonObject();

        for (Map.Entry<String, JsonElement> entry : element.getAsJsonObject().entrySet()) {
            if (entry.getValue().isJsonObject()) {
                JsonElement obj = recursiveReplace(entry.getKey(), entry.getValue(), swagger);
                if (key.equals("versions")) {
                    newObject.getAsJsonObject().add("data", obj);
                } else {
                    newObject.getAsJsonObject().add(entry.getKey().replace(".", "-"), obj);
                }
            } else {
                newObject.getAsJsonObject().add(entry.getKey().replace(".", "-"), entry.getValue());
                //Get swagger url
                if (entry.getKey().equals("swaggerUrl")) {
                    swagger.setLength(0);
                    swagger.append(entry.getValue().getAsString().trim());
                }
            }
        }

        return newObject;
    }

}
