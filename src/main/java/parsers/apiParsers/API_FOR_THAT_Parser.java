package parsers.apiParsers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import parsers.HtmlParser;
import semapi.Norm;
import semapi.NormPath;

public class API_FOR_THAT_Parser extends HtmlParser {

    public API_FOR_THAT_Parser(String name, boolean proxyEnabled) {
        super(name, proxyEnabled);
    }

    @Override
    protected List<NormPath> normalization() {
        List<NormPath> toNormalize = new ArrayList();
        toNormalize.add(new NormPath("name", Norm.toSlug));
        toNormalize.add(new NormPath("provider_url", Norm.toSite));
        toNormalize.add(new NormPath("tags/*", Norm.toSlug));
        toNormalize.add(new NormPath("categories/*", Norm.toSlug));
        return toNormalize;
    }
    
    @Override
    protected List<String> creation() {
        List<String> toCreate = new ArrayList();
        toCreate.add("tags");
        toCreate.add("categories");
        return toCreate;
    }

    @Override
    public boolean crawl(Object urls) {

        int page = 0;
        int urlSize = 0;
        int urlSizePrev = 0;

        boolean run = true;
        while (run) {
            String url = "http://www.apiforthat.com/apis?page=" + (++page);
            Document htmlDocument = readHtmlFromUrl(url);

            if (htmlDocument == null) {
                return false;
            }

            Elements links = htmlDocument.select("a[href]");
            for (Element link : links) {
                String extractedUrl = link.attr("abs:href");
                if (extractedUrl.contains("apiforthat.com/apis/")) {
                    ((Set) urls).add(extractedUrl);
                    if (++urlSize >= max && max != 0) {
                        return true;
                    }
                }
            }

            if (urlSize == urlSizePrev) {
                run = false;
            }

            urlSizePrev = urlSize;
            System.out.println("Crawled url: " + url);
        }

        return true;
    }

    @Override
    protected JsonElement parse(Object obj) {
        String url = (String) obj;
        JsonObject apiData = new JsonObject();
        Document htmlDocument = readHtmlFromUrl(url);

        if (htmlDocument == null)
            return null;
        
        //Url
        apiData.addProperty("url", url);

        //Get name
        Element apiName = htmlDocument.select("div[class=\"api-head\"] > span[itemprop=\"name\"]").first();
        if (apiName != null) {
            apiData.addProperty("name", apiName.ownText().trim());
        }

        //Get short description
        Element shortDescription = htmlDocument.select("div[class=\"api-head\"]").first();
        if (shortDescription != null) {
            apiData.addProperty("shortDescription", shortDescription.ownText().trim());
        }

        //Get homepage
        Element homepage = htmlDocument.select("div[class=\"api-head\"] > a").first();
        if (homepage != null) {
            apiData.addProperty("homepage", homepage.ownText().trim());
        }

        //Get description, category, tags, links
        Elements elements = htmlDocument.select("div[class=\"span8 show-area-api\"] > h4");
        for (Element element : elements) {
            String elem = element.ownText().trim().toLowerCase();

            if (elem.equals("description")) {
                Element description = element.nextElementSibling();
                if (description != null) {
                    apiData.addProperty("description", description.ownText().trim());
                }
            }

            if (elem.equals("category")) {
                Element category = element.nextElementSibling();
                if (category != null) {
                    String[] splitted = category.ownText().trim().split(",");
                    JsonArray array = new JsonArray();
                    for (String str : splitted) {
                        if (!str.trim().isEmpty()) {
                            array.add(str.trim());
                        }
                    }
                    apiData.add("categories", array);
                }
            }

            if (elem.equals("tags")) {
                Element tags = element.nextElementSibling();
                if (tags != null) {
                    String[] splitted = tags.ownText().trim().split(",");
                    JsonArray array = new JsonArray();
                    for (String str : splitted) {
                        if (!str.trim().isEmpty()) {
                            array.add(str.trim());
                        }
                    }
                    apiData.add("tags", array);
                }
            }

            if (elem.equals("links")) {
                Element p = element.nextElementSibling();
                do {
                    Element link = p.select("a").first();
                    String text = link.text().toLowerCase();

                    if (text.equals("api home")) {
                        apiData.addProperty("apiHome", link.absUrl("href"));
                    }

                    if (text.equals("documentation")) {
                        apiData.addProperty("documentation", link.absUrl("href"));
                    }

                    if (text.equals("pricing")) {
                        apiData.addProperty("pricing", link.absUrl("href"));
                    }

                    if (text.equals("terms and conditions")) {
                        apiData.addProperty("terms", link.absUrl("href"));
                    }
                } while ((p = p.nextElementSibling()) != null);
            }
        }

        //Get provider
        Element provider = htmlDocument.select("div[class=\"span3 show-area\"] > div[class=\"company-name\"]").first();
        if (provider != null) {
            apiData.addProperty("provider", provider.ownText().trim());
        }

        //Get provider url
        Element provider_url = htmlDocument.select("div[class=\"span3 show-area\"] > p > a").first();
        if (provider_url != null) {
            apiData.addProperty("provider_url", provider_url.absUrl("href"));
        }

        System.out.println("Parsed: " + url);
        return apiData;
    }

}
