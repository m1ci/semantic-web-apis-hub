package parsers.apiParsers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import parsers.HtmlParser;
import semapi.Filter;
import semapi.Norm;
import semapi.NormPath;
import semapi.Normalization;

public class PWEB_Parser extends HtmlParser {

    private int count = 0;
    
    public PWEB_Parser(String name, boolean proxyEnabled) {
        super(name, proxyEnabled);
    }

    @Override
    protected List<NormPath> normalization() {
        List<NormPath> toNormalize = new ArrayList();
        toNormalize.add(new NormPath("name", Norm.toSlug));
        toNormalize.add(new NormPath("api_provider", Norm.toSite));
        toNormalize.add(new NormPath("first_category/*", Norm.toSlug));
        toNormalize.add(new NormPath("second_category/*", Norm.toSlug));
        toNormalize.add(new NormPath("architectural_style/*", Norm.toSlug));
        toNormalize.add(new NormPath("request_formats/*", Norm.toSlug));
        toNormalize.add(new NormPath("response_formats/*", Norm.toSlug));
        toNormalize.add(new NormPath("auth_model/*", Norm.toSlug));
        return toNormalize;
    }

    @Override
    protected List<String> creation() {
        List<String> toCreate = new ArrayList();
        toCreate.add("auth_model");
        toCreate.add("architectural_style");
        toCreate.add("response_formats");
        toCreate.add("request_formats");
        toCreate.add("second_category");
        toCreate.add("first_category");
        return toCreate;
    }
    
    @Override
    public boolean crawl(Object obj) {

        JsonElement apis = (JsonElement) obj;
        int page = 0;
        int size = 0;

        while (true) {
            String url = "http://www.programmableweb.com/category/all/apis?page=" + (page++) + "&deadpool=1";
            Document htmlDocument = readHtmlFromUrl(url);

            if (htmlDocument == null) {
                System.out.println("Skipped: " + url);
                continue;
            }

            Element table = htmlDocument.getElementsByClass("views-table").first();
            if (table == null) {
                return true;
            }

            Iterator<Element> iterator = table.select("tbody > tr").iterator();
            while (iterator.hasNext()) {
                Element tr = iterator.next();

                JsonObject api = new JsonObject();

                Element elem_date = tr.select("td:eq(3)").first();
                if (elem_date != null) {        
                    api.addProperty("created", Normalization.toDate(elem_date.text().trim(), "MM.dd.yyyy", "yyyy-MM-dd"));
                }

                Element elem_url = tr.select("td:eq(0) > a[href]").first();
                if (elem_url != null) {
                    api.addProperty("url", elem_url.attr("abs:href"));
                    apis.getAsJsonArray().add(api);
                }
                
                if (++size >= max && max != 0) {
                    System.out.println("Crawled url: " + url);
                    return true;
                }
            }
            
            System.out.println("Crawled url: " + url);
        }
    }

    @Override
    protected JsonElement parse(Object apiData) {

        JsonObject api = ((JsonElement) apiData).getAsJsonObject();
        Document htmlDocument = readHtmlFromUrl(api.getAsJsonObject().get("url").getAsString());

        if (htmlDocument == null)
            return null;
        
        Element apiName = htmlDocument.select("div[class=\"node-header\"] > h1").first();
        if (apiName != null) {
            api.addProperty("name", apiName.ownText().trim());
        }

        Element apiDescription = htmlDocument.select("div[class=\"intro\"] > div").first();
        if (apiDescription != null) {
            api.addProperty("description", apiDescription.ownText().trim());
        }

        Element apiDeprecated = htmlDocument.select("div[class=\"deprecated\"]").first();
        if (apiDeprecated != null) {
            api.addProperty("isDeprecated", apiDeprecated.ownText().trim().contains("deprecated"));
        }
        
        Element imageUrl = htmlDocument.select("div[class=\"field-items\"] > div > img").first();
        if (imageUrl != null) {
            api.addProperty("logo_url", imageUrl.absUrl("src"));
            //apiData.logo_local_url = ImageProcessing.downloadImage(apiData.logo_url, Env.logoPath, apiData.name.replace(" ", "-").trim());
        }

        //Get API data from table
        Iterator<Element> iterator = htmlDocument.select("div[class=\"section specs\"] > div").iterator();
        Element div;
        while (iterator.hasNext()) {
            div = iterator.next();
            String label = div.select("label").text().trim().toLowerCase();
            String span = div.select("span").text().trim();

            if (label.contains("endpoint")) {
                api.addProperty("endpoint", span);
            }

            if (label.contains("home page")) {
                api.addProperty("homepage", span);
            }

            
            if (label.contains("primary category")) {
                api.add("first_category", parseString(span).getAsJsonArray());
            }
            
            if (label.contains("ssl support")) {
                api.addProperty("is_ssl_support", Filter.getBoolean(span));
            }

            if (label.contains("supported response formats")) {
                api.add("response_formats", parseString(span).getAsJsonArray());
            }

            if (label.contains("authentication model")) {
                api.add("auth_model", parseString(span).getAsJsonArray());
            }

            if (label.contains("architectural style")) {
                api.add("architectural_style", parseString(span).getAsJsonArray());
            }

            if (label.contains("docs home page url")) {
                api.add("docs_home_page", parseString(span).getAsJsonArray());
            }

            if (label.contains("support email address")) {
                api.add("email_address", parseString(span).getAsJsonArray());
            }

            if (label.contains("secondary categories")) {
                api.add("second_category", parseString(span).getAsJsonArray());
            }

            if (label.contains("api provider")) {
                api.addProperty("api_provider_name", span);
                api.addProperty("api_provider", div.select("span > a").first().absUrl("href").trim());
            }

            if (label.contains("api forum")) {
                api.addProperty("api_forum", span);
            }

            if (label.contains("twitter url")) {
                api.addProperty("twitter_url", span);
            }

            if (label.contains("developer support url")) {
                api.addProperty("support_url", span);
            }

            if (label.contains("interactive console url")) {
                api.addProperty("console_url", span);
            }

            if (label.contains("terms of service url")) {
                api.addProperty("terms_of_service_url", span);
            }

            if (label.contains("scope")) {
                api.addProperty("scope", span);
            }

            if (label.contains("device specific")) {
                api.addProperty("is_device_specific", Filter.getBoolean(span));
            }

            if (label.contains("supported request formats")) {
                api.add("request_formats", parseString(span).getAsJsonArray());
            }

            if (label.contains("is this an unofficial api?")) {
                api.addProperty("is_unofficial_api", Filter.getBoolean(span));
            }

            if (label.contains("is this a hypermedia api?")) {
                api.addProperty("is_hypermedia_api", Filter.getBoolean(span));
            }

            if (label.contains("restricted access")) {
                api.addProperty("is_restricted_access", Filter.getBoolean(span));
            }
        }

        System.out.println((++count) + " - parsed: " + api.getAsJsonObject().get("url").getAsString());
        return api;
    }

}
