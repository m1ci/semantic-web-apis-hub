package parsers;

import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import semapi.NormPath;
import semapi.Normalization;

public abstract class Parser {

    protected String name;
    protected static int max = 0;

    protected int proxyRequestIndex;
    protected boolean proxyEnabled;

    public Parser(String name, boolean proxyEnabled) {
        this.name = name;
        this.proxyRequestIndex = 0;
        this.proxyEnabled = proxyEnabled;
    }

    /*Abstract methods*/
    protected abstract JsonElement parse(Object obj);
    public abstract boolean crawl(Object obj);
    /*----------------*/

    public JsonElement runParse(Object obj) {
        JsonElement jsonObject = parse(obj);

        if (jsonObject == null) {
            return null;
        }

        for (String s : creation()) {
            Normalization.createArray(jsonObject, s);
        }

        JsonElement normalizedJsonObject = Normalization.normalize(jsonObject, normalization());
        return normalizedJsonObject;
    }

    protected List<NormPath> normalization() {
        List<NormPath> toNormalize = new ArrayList();
        return toNormalize;
    }

    protected List<String> creation() {
        List<String> toCreate = new ArrayList();
        return toCreate;
    }

    public static void setMax(int m) {
        max = m;
    }

    public static int getMax() {
        return max;
    }

    public String getName() {
        return name;
    }
}
