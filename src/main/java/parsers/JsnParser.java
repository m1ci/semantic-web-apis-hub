package parsers;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public abstract class JsnParser extends Parser {

    public JsnParser(String name) {
        super(name, false);
    }

    protected boolean readJsonFromUrl(String url, StringBuilder output) {

        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));

            String line;
            while ((line = rd.readLine()) != null) {
                output.append(line);
            }

            return true;
        } catch (Exception ex) {
            System.out.println("Error: cannot get json data from url (" + url + ")");
            return false;
        }
    }

}
